FROM debian:bookworm

WORKDIR /srv

RUN apt update && apt -y install python3 python3-pip python3-venv curl
COPY . .
RUN python3 -m venv venv
RUN venv/bin/pip3 install -r /srv/requirements.txt
RUN . venv/bin/activate
RUN ./prepare-prod-deployment.sh

ENTRYPOINT /bin/sh ./start-django.sh

