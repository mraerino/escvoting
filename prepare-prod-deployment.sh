#!/bin/sh
cd 'django_escvoting' || exit 1
NEW_SECRET_KEY=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 64)
sed -i -e "s/SECRET_KEY = .*/SECRET_KEY = '$NEW_SECRET_KEY'/" settings.py
#sed -i -e "s/DEBUG = .*/DEBUG = False/" settings.py
#sed -i -e "s/ALLOWED_HOSTS = .*/ALLOWED_HOSTS = ['escvoting.morph.sh']/" settings.py
sed -i -e "s/ALLOWED_HOSTS = .*/ALLOWED_HOSTS = ['*']/" settings.py
sed -i -e "s/STATIC_ROOT = .*/STATIC_ROOT = '\/srv\/public\/'/" settings.py
sed -i -e "s/TIME_ZONE = .*/TIME_ZONE = 'Europe\/Berlin'/" settings.py
sed -i -e "s/SESSION_COOKIE_SECURE = .*/SESSION_COOKIE_SECURE = True/" settings.py
sed -i -e "s/CSRF_COOKIE_SECURE = .*/CSRF_COOKIE_SECURE = True/" settings.py