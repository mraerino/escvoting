#!/bin/sh

. venv/bin/activate 
#cd mysite 
python manage.py migrate
python manage.py collectstatic --noinput
uwsgi --protocol uwsgi --uwsgi-socket 0.0.0.0:8042 --master -p 4 --module django_escvoting.wsgi:application