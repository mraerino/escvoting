from django.db import models
from django.core.exceptions import ValidationError

def validate_esc_points(value):
    if not 1 <= value <= 6:
        raise ValidationError(
            _("%(value)s is not an even number"),
            params={"value": value},
        )

voting_choices = [
    (1, "1"),
    (2, "2"),
    (3, "3"),
    (4, "4"),
    (5, "5"),
    (6, "6"),
]

class Song(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField("Song Name", max_length=100)
    artist = models.CharField("Artist", max_length=100)
    nationality = models.CharField("Nationality", max_length=100)
    #nationality_shorthand = models.CharField("Nationality Shorthand", max_length=3)
    nationality_emoji = models.CharField("Nationality Emoji", max_length=100)
    points_singing = models.IntegerField(default=0)
    points_performance = models.IntegerField(default=0)
    points_song = models.IntegerField(default=0)
    points_outfit = models.IntegerField(default=0)
    points_total = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.nationality_emoji} {self.artist} - {self.name}"

    def save(self, *args, **kwargs):
        self.points_total = self.points_song + self.points_singing + self.points_performance + self.points_outfit
        #self.save()
        super().save(*args, **kwargs)
    


class Vote(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    points_singing = models.IntegerField("Gesang", blank=False, default=1, choices=voting_choices)
    points_performance = models.IntegerField("Performance", blank=False, default=1, choices=voting_choices)
    points_song = models.IntegerField("Song", blank=False, default=1, choices=voting_choices)
    points_outfit = models.IntegerField("Outfit", blank=False, default=1, choices=voting_choices)

    def __str__(self):
        return f"{self.song.nationality_emoji}: {self.points_singing} / {self.points_performance} / {self.points_song} / {self.points_outfit}"

    def long_info(self):
        return f"{self.song.nationality_emoji} {self.song.nationality}: {self.points_singing} / {self.points_performance} / {self.points_song} / {self.points_outfit}"

    '''
    def save(self, *args, **kwargs):
        # v2
        self.song.points_total = self.points_song + self.points_singing + self.points_performance + self.points_outfit
        self.song.save()
        super().save(*args, **kwargs)
    '''

    # TODO: removefunktion!!