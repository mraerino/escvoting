from django.shortcuts import render
from django.template import loader
from django.contrib import messages

from django.http import HttpResponse

from .forms import VoteForm
from .models import Song, Vote

# helper
def calculate_results_correctly():
    for song in Song.objects.all():
        song.points_outfit = 0
        song.points_performance = 0
        song.points_singing = 0
        song.points_song = 0
        song.points_total = 0
        song.save()
    # TODO: refactoren - kann das in den song-loop mit rein?
    for vote in Vote.objects.all():
        vote.song.points_outfit += vote.points_outfit
        vote.song.points_performance += vote.points_performance
        vote.song.points_singing += vote.points_singing
        vote.song.points_song += vote.points_song
        vote.song.save()

# Create your views here.
def index(request):
    if request.method == "POST":
        form = VoteForm(request.POST)
        if form.is_valid():
            new_thing = form.save()
            messages.success(request, f"Dein Vote wurde gespeichert! {new_thing.long_info()}")
        form = VoteForm()

    elif request.method == "GET":
        form = VoteForm()
        print(form)
    return render(request, "index.html", {'form': form})

def results(request):
    if request.method == "GET":
        calculate_results_correctly()
        results = {}
        results['points_singing'] = Song.objects.all().order_by('-points_singing').filter(points_singing__gt=0)[:3]
        results['points_performance'] = Song.objects.all().order_by('-points_performance').filter(points_performance__gt=0)[:3]
        results['points_song'] = Song.objects.all().order_by('-points_song').filter(points_song__gt=0)[:3]
        results['points_outfit'] = Song.objects.all().order_by('-points_outfit').filter(points_outfit__gt=0)[:3]
        results['points_total'] = Song.objects.all().order_by('-points_total').filter(points_total__gt=0)[:3]

    return render(request, "results.html", {'results': results})

def results_all(request):
    if request.method == "GET":
        calculate_results_correctly()
        results = {}
        results['points_singing'] = Song.objects.all().order_by('-points_singing').filter(points_singing__gt=0)
        results['points_performance'] = Song.objects.all().order_by('-points_performance').filter(points_performance__gt=0)
        results['points_song'] = Song.objects.all().order_by('-points_song').filter(points_song__gt=0)
        results['points_outfit'] = Song.objects.all().order_by('-points_outfit').filter(points_outfit__gt=0)
        results['points_total'] = Song.objects.all().order_by('-points_total').filter(points_total__gt=0)

    return render(request, "results-all.html", {'results': results})