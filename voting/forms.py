from django import forms
from .models import Song, Vote

class VoteForm(forms.ModelForm):
    class Meta:
        model = Vote
        fields = [
            "song",
            "points_singing",
            "points_performance",
            "points_song",
            "points_outfit",
        ]
        widgets = {
            "points_singing": forms.RadioSelect,
            "points_song": forms.RadioSelect,
            "points_outfit": forms.RadioSelect,
            "points_performance": forms.RadioSelect,
        }
