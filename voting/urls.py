from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("results", views.results, name="Results"),
    path("results/all", views.results_all, name="Results"),
]